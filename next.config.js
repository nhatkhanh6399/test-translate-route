const nextTranslate = require("./src/modules/i18n/next.config");
/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
};

module.exports = nextTranslate(nextConfig);
