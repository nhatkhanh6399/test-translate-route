import useTranslation from "next-translate/useTranslation";

import React from "react";
import { useRouter } from "next/router";
import I18nLink from "./Link";

const ChangeLanguage = () => {
	const { t } = useTranslation();
	const { locale, locales } = useRouter();

	if (!locales) return null;

	return locales.map((lng) => {
		if (lng === locale) return null;

		return (
			<I18nLink href="/" locale={lng} key={lng}>
				{t(`common:cta`, { lang: lng })}
			</I18nLink>
		);
	});
};

export default ChangeLanguage;
