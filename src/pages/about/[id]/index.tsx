import React from "react";
import I18nLink from "../../../modules/i18n/components/Link";

export const permalinks: { [key: string]: string } = {
	en: "/about/:id",
	vi: "/gioi-thieu/:id",
};

const Detail = () => {
	return (
		<div>
			<I18nLink href="/about">Back to about</I18nLink>
			Detail
		</div>
	);
};

export default Detail;
