import React from "react";
import I18nLink from "../../modules/i18n/components/Link";

export const permalinks: { [key: string]: string } = {
	en: "/about",
	vi: "/gioi-thieu",
};

const About = () => {
	return (
		<div>
			<I18nLink href="/">Back to home</I18nLink>

			<div>
				<div style={{ color: "red" }}>
					<I18nLink href="/about/1">to detail</I18nLink>
				</div>
			</div>
		</div>
	);
};

export default About;
